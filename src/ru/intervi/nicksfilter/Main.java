package ru.intervi.nicksfilter;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	private final Config CONF = new Config(this);
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPreLogin(PlayerLoginEvent event) {
		if (!CONF.enabled || (CONF.newbie && event.getPlayer().hasPlayedBefore())) return;
		String name = event.getPlayer().getName().toLowerCase();
		for (String regex : CONF.regex) {
			if (name.indexOf(regex) != -1 || name.matches(regex)) {
				event.disallow(Result.KICK_OTHER, CONF.kickmess);
				break;
			}
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender.hasPermission("nicksfilter.reload")) {
			CONF.load();
			sender.sendMessage(CONF.reloaded);
		} else sender.sendMessage(CONF.noperm);
		return true;
	}
}
