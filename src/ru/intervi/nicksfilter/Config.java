package ru.intervi.nicksfilter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
	public Config(Main main) {
		MAIN = main;
		load();
	}
	
	private final Main MAIN;
	
	public boolean enabled = true;
	public boolean newbie = true;
	public String kickmess = "&cЭтот ник запрещён! Зайди с другим.";
	public String reloaded = "&eконфиг перезагружен";
	public String noperm = "&cнет прав";
	public List<String> regex = new ArrayList<String>();
	
	public static String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public void load() {
		MAIN.saveDefaultConfig();
		MAIN.reloadConfig();
		FileConfiguration conf = MAIN.getConfig();
		enabled = conf.getBoolean("enabled");
		newbie = conf.getBoolean("newbie");
		kickmess = color(conf.getString("kickmess"));
		reloaded = color(conf.getString("reloaded"));
		noperm = color(conf.getString("noperm"));
		regex = conf.getStringList("regex");
	}
}
